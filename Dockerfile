FROM python:3-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
RUN pip install flask --no-cache-dir
COPY . /usr/src/app
EXPOSE 80
CMD [ "python", "./app.py" ]